import express from 'express';

import * as usersController from '../controllers/users-controller';

const router = express.Router();

// List all employees
router.get('/', usersController.getUsers);

// Get a single employee by ID
router.get('/:userid', usersController.getUserDetails);

// Create a new employee
router.post('/', usersController.createUser);

// Update an employee by ID
router.put('/:userid', usersController.updateUser);

// Delete an employee by ID
router.delete('/:userid', usersController.deleteUser);

export default router;