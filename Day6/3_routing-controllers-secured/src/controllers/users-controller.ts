import { Body, Delete, Get, JsonController, NotFoundError, Param, Post, Put, QueryParam, UseBefore } from "routing-controllers";
import { findAndDeleteUser, findAndUpdateUser, getUsers as getAllUsers, getUser, insertUser } from '../data-access';
import { createUserSchema, updateUserSchema } from "../validation/schema";
import { User } from "../models/user";
import { authenticateJWT } from "../middlewares/jwt-auth-middleware";

@JsonController('/users')
@UseBefore(authenticateJWT)
export class UserController {
    @Get('/')
    async getUsers(@QueryParam("page") page: number, @QueryParam("limit") limit: number) {
        try {
            const result = await getAllUsers(page, limit);
            return { users: result.users, totalPages: Math.ceil(result.total / limit) };
        } catch (error) {
            throw new Error('Failed to get users');
        }
    }

    @Get('/:userid')
    async getUserDetails(@Param('userid') userid: number) {
        const user = await getUser(userid);
        if (user) {
            return user;
        } else {
            throw new NotFoundError("User not found");
        }
    }

    @Post('/')
    async createUser(@Body() body: any) {
        try {
            const value = await createUserSchema.validateAsync(body);
            const user = new User(value.userid, value.name, value.email);
            await insertUser(user);
            return { message: 'User created successfully', user };
        } catch (error) {
            throw error;
        }
    }

    @Put('/:userid')
    async updateUser(@Param('userid') userid: number, @Body() body: any) {
        try {
            const value = await updateUserSchema.validateAsync(body);
            const user = new User(value.userid, value.name, value.email);
            await findAndUpdateUser(userid, user);
            return { message: 'User updated successfully', user };
        } catch (error) {
            throw error;
        }
    }

    @Delete('/:userid')
    async deleteUser(@Param('userid') userid: number) {
        try {
            await findAndDeleteUser(userid);
            return { message: 'User deleted successfully' };
        } catch (error) {
            throw error;
        }
    }
}