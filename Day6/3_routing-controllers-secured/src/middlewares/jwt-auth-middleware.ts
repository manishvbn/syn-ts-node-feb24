import { expressjwt } from 'express-jwt';

export const authenticateJWT = expressjwt({ secret: process.env.JWT_SECRET, algorithms: ['HS256'] });



// import { Request, Response, NextFunction } from 'express';
// function ValidateMiddleware(req: Request, res: Response, next: NextFunction) {
//     const token = req.header('auth-token');
//     if (!token) return res.status(401).send('Access Denied');

//     try {
//         jwt.verify(token, process.env.JWT_SECRET);
//         next();
//     } catch (err) {
//         res.status(400).send('Invalid Token');
//     }
// }