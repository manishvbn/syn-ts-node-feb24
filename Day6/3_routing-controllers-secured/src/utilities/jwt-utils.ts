import * as jwt from 'jsonwebtoken';

export const generateToken = (payload: any): string => {
    // return jwt.sign(payload, process.env.JWT_SECRET, { expiresIn: '1h', algorithm: 'HS256' });
    return jwt.sign(payload, process.env.JWT_SECRET, { expiresIn: 30, algorithm: 'HS256' });
}