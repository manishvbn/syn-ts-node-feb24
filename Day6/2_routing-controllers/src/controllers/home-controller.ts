import { Controller, Get, Render } from "routing-controllers";

@Controller()
export class HomeController {
    @Get('/')
    @Render('home/index')
    index() {
        return { pageTitle: 'Index View' };
    }

    @Get('/contact')
    @Render('home/contact')
    contact() {
        return { pageTitle: 'Contact View' };
    }

    @Get('/about')
    @Render('home/about')
    about() {
        return { pageTitle: 'About View' };
    }
}
