import express from 'express';
import path from 'path';
import favicon from 'serve-favicon';
import logger from 'morgan';
import 'dotenv/config';
import './config/data-source';
import swaggerUi from 'swagger-ui-express';
import * as swaggerDocument from './config/swagger.json';
import 'reflect-metadata';

import { useExpressServer } from 'routing-controllers';
// import { createExpressServer } from 'routing-controllers';

const app = express();

// const app = createExpressServer({
//     controllers: [__dirname + '/controllers/*.ts'],
//     middlewares: [__dirname + '/middlewares/*.ts']
// });

app.set('view engine', 'pug');

app.use('/my-docs', swaggerUi.serve, swaggerUi.setup(swaggerDocument));

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(express.static(path.join(process.cwd(), 'public')));
app.use(favicon(path.join(process.cwd(), 'public', 'images', 'favicon.png')));

useExpressServer(app, {
    controllers: [__dirname + '/controllers/*.ts'],
    middlewares: [__dirname + '/middlewares/*.ts']
});

export default app;