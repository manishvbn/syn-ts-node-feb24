import { Redis } from "ioredis";
import { JsonController, Get } from "routing-controllers";
import redisClient from "../config/redis";

@JsonController('/operations')
export class OperationsController {
    private redisClient: Redis;

    constructor() {
        this.redisClient = redisClient;
    }

    @Get('/')
    async getAllOperations() {
        try {
            const streamData = await this.redisClient.xrange('user-stream', '-', '+');

            const operations = streamData.map(([messageId, fields]) => ({
                messageId,
                payload: JSON.parse(fields[0]), // Assuming payload is at index 0 and parsing it if it's a JSON string
                message: fields[1] // Assuming message is at index 1
            }));

            return operations;
        } catch (error) {
            console.error('Error retrieving operations from Redis Stream:', error);
            throw new Error('Unable to retrieve operations');
        }
    }
}