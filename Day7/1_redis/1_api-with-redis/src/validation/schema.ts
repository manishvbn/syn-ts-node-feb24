import Joi from 'joi';

type UserSchema = {
    userid: number;
    name: string;
    email: string;
};

export const createUserSchema = Joi.object<UserSchema>({
    userid: Joi.number().required(),
    name: Joi.string().min(3).required(),
    email: Joi.string().email().required()
}).required();

export const updateUserSchema = Joi.object<UserSchema>({
    userid: Joi.number().required(),
    name: Joi.string().min(3).required(),
    email: Joi.string().email().required()
}).min(1);