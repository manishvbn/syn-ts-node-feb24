import { User } from "../models/user";
import { AppDataSource } from "../config/data-source";
import { UserEntity } from "../entity/user";

const userRepository = AppDataSource.getRepository(UserEntity);

export const getUsers = async (page: number | undefined, limit: number | undefined): Promise<{ users: User[], total: number }> => {
    let skip: number | undefined;
    let take: number | undefined;

    if (page !== undefined && limit !== undefined) {
        skip = (page - 1) * limit;
        take = limit;
    }

    let users;
    let total;
    
    if (skip !== undefined && take !== undefined) {
        [users, total] = await userRepository.findAndCount({ skip, take });
    } else {
        [users, total] = await userRepository.findAndCount();
    }
    
    return {
        users: users.map((entity) => new User(entity.userId, entity.name, entity.email)),
        total: total
    };
}

export const getUser = async (id: number): Promise<User | null> => {
    const userEntity = await userRepository.findOneBy({ userId: id });
    if (userEntity) {
        return new User(userEntity.userId, userEntity.name, userEntity.email);
    }
    return null;
}

export const insertUser = async (userToInsert: User): Promise<User> => {
    const userEntity = userRepository.create({
        userId: userToInsert.id,
        name: userToInsert.name,
        email: userToInsert.email
    });

    await userRepository.save(userEntity);
    return new User(userEntity.userId, userEntity.name, userEntity.email);
}

export const findAndUpdateUser = async (id: number, userToUpdate: Partial<User>): Promise<User | null> => {
    let userEntity = await userRepository.findOneBy({ userId: id });
    if (userEntity) {
        userEntity = userRepository.merge(userEntity, userToUpdate);
        // userEntity.name = userToUpdate.name ?? userEntity.name;
        await userRepository.save(userEntity);
        return new User(userEntity.userId, userEntity.name, userEntity.email);
    }
    return null;
}

export const findAndDeleteUser = async (id: number): Promise<void> => {
    let userEntity = await userRepository.findOneBy({ userId: id });
    if (!userEntity) {
        throw new Error("User not found");
    }

    await userRepository.delete({ userId: id });
}