import { Redis } from "ioredis";

const MAX_RETRIES = process.env.redisMaxRetries ? parseInt(process.env.redisMaxRetries) : 2;

let retries = 0;

const redisClient = new Redis({
    host: process.env.redisHost || "localhost",
    port: process.env.redisPort ? parseInt(process.env.redisPort) : 6379,
    password: process.env.redisPassword || undefined,
    retryStrategy: (times) => {
        if (times < MAX_RETRIES)
            return 5000;
        else
            return null;
    }
});

redisClient.on('connect', () => {
    console.log('Connected to Redis successfully');
});

redisClient.on('error', (error) => {
    // console.log(error);
    retries++;
    if (retries < MAX_RETRIES) {
        console.error(`Error connecting to Redis server. Retry attempt ${retries}/${MAX_RETRIES}`);
    } else {
        console.error(`Maximum number of retries (${MAX_RETRIES}) reached. Unable to connect to Redis server.`);
    }
});

export default redisClient;