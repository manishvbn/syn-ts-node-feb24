import redisClient from "./config/redis";

export async function readMessages(lastId="$") {
    try {
        const results = await redisClient.xread('BLOCK', 0, 'STREAMS', 'user-stream', lastId);

        const [key, messages] = results[0];

        messages.forEach((message) => {
            console.log("Stream: %s --> Id: %s. Data: %O", key, message[0], message[1]);
            // Logic to process the message
        });

        await readMessages(messages[messages.length - 1][0]);
    } catch(error) {
        console.error('Error reading messages from Redis Stream:', error);
    }
}