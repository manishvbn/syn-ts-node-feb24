import { ObjectId } from "mongodb";
import { Column, Entity, ObjectIdColumn } from "typeorm";

@Entity({ name: 'users' })
export class UserEntity {
    @ObjectIdColumn()
    _id: ObjectId;

    @Column({ unique: true })
    userId: number;

    @Column()
    name: string;

    @Column()
    email: string;
}