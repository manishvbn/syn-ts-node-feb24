import { DataSource } from "typeorm";
import "reflect-metadata";
import { UserEntity } from "../entity/user";

export const AppDataSource: DataSource = new DataSource({
    type: "mongodb",
    url: process.env.mongoURI,
    useNewUrlParser: true,
    useUnifiedTopology: true,
    database: "EmployeeDB_Sync_Feb24",
    entities: [UserEntity],
    synchronize: true,
    logging: true
});

AppDataSource.initialize().then(async () => {
    console.log("MongoDB DataSource has been initialized successfully.");
}).catch((err) => {
    console.error("Error during MongoDB DataSource initialization:", err);
});