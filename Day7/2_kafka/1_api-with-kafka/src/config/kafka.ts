import { Kafka, Message, Producer, ProducerBatch, TopicMessages, logLevel } from "kafkajs";

interface MessageFormat { id: number, name: string, email: string }

export default class KafkaProducer {
    private static instance: KafkaProducer;
    private producer: Producer;

    private constructor() {
        this.producer = this.createProducer();
    }

    private createProducer(): Producer {
        const kafka = new Kafka({
            clientId: 'user-app',
            brokers: ['localhost:9092'],
            logLevel: logLevel.ERROR
        });

        return kafka.producer();
    }

    public static getInstance(): KafkaProducer {
        if (!KafkaProducer.instance) {
            KafkaProducer.instance = new KafkaProducer();
        }
        return KafkaProducer.instance;
    }

    public async start(): Promise<void> {
        try {
            await this.producer.connect();
            console.log("Connected to Kafka Producer");
        } catch (error) {
            console.error('Error Connecting to the Kafka Producer', error);
        }
    }

    public async shutdown(): Promise<void> {
        await this.producer.disconnect();
        console.log("Disconnected from Kafka Producer");
    }

    public async sendBatch(messages: Array<MessageFormat>): Promise<void> {
        const kafkaMessages: Array<Message> = messages.map((message) => {
            return {
                value: JSON.stringify(message)
            };
        });

        const topicMessages: TopicMessages = {
            topic: 'user-topic',
            messages: kafkaMessages
        }

        const batch: ProducerBatch = {
            topicMessages: [topicMessages]
        }

        await this.producer.sendBatch(batch);
    }
}