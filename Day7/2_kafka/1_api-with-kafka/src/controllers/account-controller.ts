import { Body, JsonController, Post } from "routing-controllers";
import { generateToken } from "../utilities/jwt-utils";

@JsonController()
export class AccountController {
    @Post('/login')
    login(@Body() credentials: { username: string, password: string }) {
        // Your logic here to authenticate the user and generate a token

        if (credentials.username !== 'manishs' || credentials.password !== 'manishs') {
            throw new Error('Invalid credentials!');
        } else {
            const token = generateToken({ username: credentials.username });
            return { token };
        }
    }
}