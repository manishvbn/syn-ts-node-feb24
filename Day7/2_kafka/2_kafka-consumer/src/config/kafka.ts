import { Kafka, Consumer, EachMessagePayload, logLevel } from 'kafkajs';

export default class KafkaConsumer {
  private static instance: KafkaConsumer;
  private consumer: Consumer;

  private constructor() {
    this.consumer = this.createConsumer();
  }

  public static getInstance(): KafkaConsumer {
    if (!KafkaConsumer.instance) {
      KafkaConsumer.instance = new KafkaConsumer();
    }
    return KafkaConsumer.instance;
  }

  public async start(): Promise<void> {
    try {
      await this.consumer.connect();
      console.log('Consumer connected');
      await this.consumer.subscribe({ topic: 'user-topic' });
      await this.consumer.run({
        eachMessage: this.processMessage.bind(this)
      });
    } catch (error) {
      console.error('Error connecting the consumer:', error);
    }
  }

  public async shutdown(): Promise<void> {
    await this.consumer.disconnect();
  }

  private async processMessage({ message }: EachMessagePayload): Promise<void> {
    const value = JSON.parse(message.value?.toString() || '');
    console.log('Received message:', value);
    // Process the received message as needed
  }

  private createConsumer(): Consumer {
    const kafka = new Kafka({
      clientId: 'user-app',
      brokers: ['localhost:9092'],
      logLevel: logLevel.ERROR
    });

    return kafka.consumer({ groupId: 'user-group' });
  }
}
