function hello_js(name) {
    console.log("Hello " + name);
}

hello_js("JavaScript");
hello_js(10);
hello_js("Abhijeet", "Pune");
hello_js();