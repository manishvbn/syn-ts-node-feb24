// // Fn Declaration
// function add1(a, b) {
//     return a + b;
// }

// // Fn Expression
// var add2 = function (a, b) {
//     return a + b;
// }

// // Arrow Fn
// var add3 = (a, b) => a + b;

// console.log(add1(2, 3));
// console.log(add2(2, 3));
// console.log(add3(2, 3));

// -----------------------------------------------------

var i = 10;
console.log("i is: ", i);
console.log("typeof i is: ", typeof i);

var f = function () { console.log("Hello"); };
console.log("f is: ", f);
console.log("typeof f is: ", typeof f);

// A function is a type in JavaScript which can refer to a block of code