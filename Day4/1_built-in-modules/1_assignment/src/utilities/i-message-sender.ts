export interface IMessageSender {
    send(message: string): void;
}