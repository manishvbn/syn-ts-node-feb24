import { Email } from "./email";
import { SMS } from "./sms";

export const emailClient = new Email();
export const smsClient = new SMS();